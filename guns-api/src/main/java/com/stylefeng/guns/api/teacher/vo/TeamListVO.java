package com.stylefeng.guns.api.teacher.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class TeamListVO implements Serializable {
    private Integer teamId;
    private String priority;
    private String professional;
    private List<TeamInfoDetaVO> members;
}
