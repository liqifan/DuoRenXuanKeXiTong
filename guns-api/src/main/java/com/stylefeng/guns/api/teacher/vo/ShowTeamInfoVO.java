package com.stylefeng.guns.api.teacher.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ShowTeamInfoVO implements Serializable {

    private List<TeamInfoVO> content;
}
