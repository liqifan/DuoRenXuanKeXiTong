package com.stylefeng.guns.api.student.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class GetTeamVO implements Serializable {

    private Integer tid;
    /**
     * 验证码
     */
    private Integer code;
}
