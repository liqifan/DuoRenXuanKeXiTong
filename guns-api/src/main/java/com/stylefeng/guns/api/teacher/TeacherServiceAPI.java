package com.stylefeng.guns.api.teacher;

import com.stylefeng.guns.api.teacher.vo.ShowTeamInfoVO;
import com.stylefeng.guns.api.teacher.vo.SubjectInfoVO;
import java.util.List;

public interface TeacherServiceAPI {
    ////判断该题目是否已选择好学生
    Integer hadSelected(int suid);
    //更改课题状态
    int updateSubjectStatus(int suid);

    //添加课题
    int insertSubject(SubjectInfoVO subjectInfoVO);

    //显示我的未选定学生的课题
    List<SubjectInfoVO> getSubjectsByUserId(int userId);

    //显示我的所有课题
    List<SubjectInfoVO> getSubjectsByUserId3(int userId);

    //编辑课题
    int updateSubject(SubjectInfoVO subjectInfoVO);

    //删除课题
    int delSubject(int suid);

    //获取对应题目及其下的队伍信息
    ShowTeamInfoVO getTeamInfoWithSub(int suid);

    //显示我的学生
    ShowTeamInfoVO getMyStu(int userId);
    /**
     * 更新ti表为选中状态-》删除ti表中该队伍的其他志愿
     * @return
     */
    int updateInten(int suid, int teamId);
    int delIntention(int suid, int teamId);


    //导入excel
    List getBankListByExcel(byte[] bytes, String fileName) throws Exception;
}
