package com.stylefeng.guns.api.teacher.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class TeamInfoDetaVO implements Serializable {

//    private int teamId;
    private String name;
    private String personalIntro;
//    private String priority;
    private String professional;
}
