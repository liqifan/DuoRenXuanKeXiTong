package com.stylefeng.guns.api.teacher.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class TeamInfoVO implements Serializable {

    private Integer suid;

//    private Integer teamId;

    private String title;

    private String type;

//    private String priority;

//    private String professional;

    private List<TeamListVO> teamList;

}
