package com.stylefeng.guns.api.student.vo;

import com.sun.scenario.effect.impl.prism.PrImage;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class TeamMemInfoVO implements Serializable {

    private Integer code;
    private List<String> name;

}
