package com.stylefeng.guns.rest.modular.teacher;

import com.alibaba.dubbo.config.annotation.Reference;
import com.stylefeng.guns.api.teacher.TeacherServiceAPI;
import com.stylefeng.guns.api.teacher.vo.ShowTeamInfoVO;
import com.stylefeng.guns.api.teacher.vo.SubjectInfoVO;
import com.stylefeng.guns.api.teacher.vo.TeamInfoVO;
import com.stylefeng.guns.core.util.ExcelImportUtils;
import com.stylefeng.guns.rest.modular.vo.ResponseVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RequestMapping("/gra/teacher/")
@RestController
@CrossOrigin
public class TeacherController {

    @Reference(interfaceClass = TeacherServiceAPI.class,check = false)
    private TeacherServiceAPI teacherServiceAPI;

    /**
     * 添加题目
     * @param subjectInfoVO
     * @return
     */
    @PostMapping(value = "insertSubject")
    public ResponseVO insertSubject(SubjectInfoVO subjectInfoVO){
        int i = teacherServiceAPI.insertSubject(subjectInfoVO);
        if(i <= 0){
            return ResponseVO.serviceFail("添加课题失败，请刷新重试");
        }
        return ResponseVO.success("添加成功");
    }

    /**
     * 获取题目
     * @param userId
     * @return
     */
    @GetMapping(value = "getSubjects")
    public ResponseVO getSubjects(int userId){
        List<SubjectInfoVO> subjectsByUserId = teacherServiceAPI.getSubjectsByUserId3(userId);
        if(subjectsByUserId == null || subjectsByUserId.size() <=0){
            return ResponseVO.success("您还未添加课题！");
        }
        return ResponseVO.success(subjectsByUserId);
    }

    /**
     * 编辑题目
     * @param subjectInfoVO
     * @return
     */
    @PostMapping(value = "updateSubject")
    public ResponseVO updateSubject(SubjectInfoVO subjectInfoVO){
        int i = teacherServiceAPI.updateSubject(subjectInfoVO);
        if(i <= 0 ){
            return ResponseVO.serviceFail("更新课题失败");
        }
        return ResponseVO.success("编辑成功");
    }

    /**
     * 删除题目
     * @param suid
     * @return
     */
    @GetMapping(value = "delSubject")
    public ResponseVO delSubject(int suid){
        int i = teacherServiceAPI.delSubject(suid);
        if(i <= 0 ){
            return ResponseVO.serviceFail("删除课题失败");
        }
        return ResponseVO.success("删除成功");
    }

    /**
     * 我的题目界面
     * @param userId
     * @return
     */
    @GetMapping(value = "getTeamInfoWithSub")
    public ResponseVO getTeamInfoWithSub(int userId){
        ShowTeamInfoVO teamInfoWithSub = teacherServiceAPI.getTeamInfoWithSub(userId);
        if(teamInfoWithSub == null){
            return ResponseVO.serviceFail("获取信息失败，请刷新重试");
        }
        return ResponseVO.success(teamInfoWithSub);
    }

    /**
     * 获取我的学生信息
     * @param userId
     * @return
     */
    @GetMapping(value = "getMyStu")
    public ResponseVO getMyStu(int userId){
        ShowTeamInfoVO teamInfoWithSub = teacherServiceAPI.getMyStu(userId);
        if(teamInfoWithSub == null){
            return ResponseVO.serviceFail("您还未选择学生");
        }
        return ResponseVO.success(teamInfoWithSub);
    }

    /**
     * 选择学生
     * @param suid
     * @param teamId
     * @return
     */
    @PostMapping(value = "chooseStu")
    @Transactional
    public ResponseVO chooseStu(int suid,int teamId){
        //判断该题目是否已选择好学生
        Integer i3 = teacherServiceAPI.hadSelected(suid);
        if(i3 !=null && i3 >0){
            return ResponseVO.serviceFail("该题目已经选定学生，无法再选！");
        }
        int i = teacherServiceAPI.updateInten(suid, teamId);
        if(i <= 0){
            return ResponseVO.serviceFail("提交失败，请刷新重试");
        }
        int i1 = teacherServiceAPI.delIntention(suid, teamId);
        int i2 = teacherServiceAPI.updateSubjectStatus(suid);
        if(i2 <=0){
            return ResponseVO.serviceFail("提交失败，请刷新重试");
        }
        return ResponseVO.success("提交成功");
    }

    @PostMapping(value = "/upload")
    public String uploadExcel(@RequestParam("file") MultipartFile file) throws Exception {
        if (file.isEmpty()) {
            return "文件不能为空";
        }
        byte[] buffer = file.getBytes();
        InputStream inputStream = file.getInputStream();
        List<List<Object>> list = teacherServiceAPI.getBankListByExcel(buffer, file.getOriginalFilename());
        inputStream.close();

        for (int i = 0; i < list.size(); i++) {
            List<Object> lo = list.get(i);
            //TODO 随意发挥
            System.out.println(lo);

        }
        return "上传成功";
    }
}
