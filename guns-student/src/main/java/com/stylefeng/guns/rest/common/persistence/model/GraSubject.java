package com.stylefeng.guns.rest.common.persistence.model;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author qifanlee
 * @since 2019-05-10
 */
@TableName("gra_subject")
@Data
public class GraSubject extends Model<GraSubject> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "suid", type = IdType.AUTO)
    private Integer suid;
    /**
     * 题目名称
     */
    private String title;
    /**
     * 题目类别
     */
    private String type;
    /**
     * 指导老师
     */
    private String teacher;
    /**
     * 已选人数
     */
    @TableField("selected_num")
    private int selectedNum;
    /**
     * 限选人数
     */
    @TableField("limited_num")
    private int limitedNum;
    /**
     * 内容
     */
    private String content;

    /**
     * 状态
     */
    private int status;


    @Override
    protected Serializable pkVal() {
        return this.suid;
    }
}
