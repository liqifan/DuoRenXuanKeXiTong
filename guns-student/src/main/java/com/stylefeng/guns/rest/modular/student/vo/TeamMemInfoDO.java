package com.stylefeng.guns.rest.modular.student.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class TeamMemInfoDO implements Serializable {

    private Integer code;
    private String name;

}
