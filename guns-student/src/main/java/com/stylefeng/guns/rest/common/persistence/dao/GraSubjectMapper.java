package com.stylefeng.guns.rest.common.persistence.dao;

import com.stylefeng.guns.rest.common.persistence.model.GraSubject;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author qifanlee
 * @since 2019-05-10
 */
public interface GraSubjectMapper extends BaseMapper<GraSubject> {
    @Select("SELECT\n" +
            "\tsuid,\n" +
            "\ttitle,\n" +
            "\ttype,\n" +
            "\tteacher,\n" +
            "\tselected_num AS selectedNum,\n" +
            "\tlimited_num AS limitedNum,\n" +
            "\tcontent,\n" +
            "\t`status`\n" +
            "FROM\n" +
            "\tgra_subject\n" +
            "WHERE\n" +
            "\t`status` = 0")
    List<GraSubject> getAllSubjects();

    List<GraSubject> getSubjectsByTeamId(int teamId);

    //查询题目
    List<GraSubject> getSubjectsByKeyword(String keyword);

    //更新题目已选人数
    int updateHadSelected(List<GraSubject> graSubjects);

    //显示确认的题目
    @Select("SELECT\n" +
            "\ts.*\n" +
            "FROM\n" +
            "\tgra_subject s,\n" +
            "\tgra_team_intention ti\n" +
            "WHERE\n" +
            "   ti.sid = s.suid\n" +
            "AND ti.selected =99\n" +
            "AND ti.tid = #{teamId}")
    GraSubject getSureSub(int teamId);
}
