package com.stylefeng.guns.rest.common.persistence.dao;

import com.stylefeng.guns.api.teacher.vo.TeamInfoDetaVO;
import com.stylefeng.guns.api.teacher.vo.TeamListVO;
import com.stylefeng.guns.rest.common.persistence.model.GraSubject;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author qifanlee
 * @since 2019-05-10
 */
public interface GraSubjectMapper extends BaseMapper<GraSubject> {

    //判断该题目是否已选择好学生
    @Select("select id from gra_team_intention where sid=#{suid} and selected=99")
    Integer hadSelected(int suid);
    //更改题目状态
    @Update("update gra_subject set status = 1 where suid = #{suid}")
    int updateSubjectStatus(int suid);

    //添加课题
    @Insert("insert into gra_subject (title,type,teacher,selected_num,limited_num,content) values(#{title},#{type},#{teacher},#{selectedNum},#{limitedNum},#{content})")
    int insertSubject(GraSubject graSubject);

    /**
     * 获取对应题目及其下的队伍信息
     */
    //显示未被选择的课题
    List<GraSubject> getSubjectsByUserId(int userId);
    //显示我的已被选择的课题
    List<GraSubject> getSubjectsByUserId2(int userId);
    //显示我的所有课题
    List<GraSubject> getSubjectsByUserId3(int userId);
    //获取所属题目下的队伍信息
    List<TeamInfoDetaVO> getTeamInfoWithSub(int suid);

    //我的学生页
    List<TeamInfoDetaVO> getMyStuInfoWithSub(int suid);

    //获取选择我这门课的所有队伍及其志愿顺序
    List<TeamListVO> getTeamInfo(int suid);

    //获取被我选定的那支队伍
    @Select("SELECT DISTINCT\n" +
            "\tti.tid AS teamId,\n" +
            "\tCONCAT('第', ti.priority, '志愿') AS priority,\n" +
            "\tu.professional AS professional\n" +
            "FROM\n" +
            "\tgra_team_intention ti,\n" +
            "\tgra_user u\n" +
            "WHERE\n" +
            "\tu.teamId = ti.tid\n" +
            "AND selected = 99\n" +
            "AND ti.sid = #{suid}")
    List<TeamListVO> getTeam(int suid);

    //根据队伍id获取所有成员信息
    @Select("SELECT\n" +
            "\tname as `name`,\n" +
            "\tintroduce as personalIntro,\n" +
            "\tprofessional\n" +
            "FROM\n" +
            "\tgra_user u,\n" +
            "\tgra_user_expand ue\n" +
            "WHERE\n" +
            "\tue.userId = u.id\n" +
            "AND teamId = #{tid}")
    List<TeamInfoDetaVO> getMemberInfo(int tid);

}
