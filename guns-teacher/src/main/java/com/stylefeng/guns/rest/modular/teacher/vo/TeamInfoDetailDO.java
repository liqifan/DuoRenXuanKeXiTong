package com.stylefeng.guns.rest.modular.teacher.vo;

import lombok.Data;

@Data
public class TeamInfoDetailDO {

    private String name;
    private String introduce;
}
