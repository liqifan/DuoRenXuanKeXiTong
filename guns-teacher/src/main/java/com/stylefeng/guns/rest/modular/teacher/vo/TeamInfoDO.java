package com.stylefeng.guns.rest.modular.teacher.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class TeamInfoDO{

    private Integer suid;

    private Integer teamId;

    private String title;

    private String type;

    private List<TeamInfoDetailDO> membersInfo;

}
