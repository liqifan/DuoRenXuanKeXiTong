package com.stylefeng.guns.rest.modular.teacher;

import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.stylefeng.guns.api.teacher.TeacherServiceAPI;
import com.stylefeng.guns.api.teacher.vo.*;
import com.stylefeng.guns.rest.common.persistence.dao.*;
import com.stylefeng.guns.rest.common.persistence.model.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.*;


@Slf4j
@Component
@Service(interfaceClass = TeacherServiceAPI.class)
public class TeacherServiceImpl implements TeacherServiceAPI {

    @Autowired
    private GraSubjectMapper graSubjectMapper;

    @Autowired
    private GraTeamIntentionMapper graTeamIntentionMapper;

    @Autowired
    private GraUserMapper graUserMapper;

    @Override
    public Integer hadSelected(int suid) {
        Integer i = graSubjectMapper.hadSelected(suid);
        return i;
    }

    @Override
    public int updateSubjectStatus(int suid) {
        int i = graSubjectMapper.updateSubjectStatus(suid);
        return i;
    }

    @Override
    public int insertSubject(SubjectInfoVO subjectInfoVO) {
        //VO 转 DO
        GraSubject graSubject = do2GraSubject(subjectInfoVO);
        int i = graSubjectMapper.insertSubject(graSubject);
        return i;
    }

    @Override
    public List<SubjectInfoVO> getSubjectsByUserId(int userId) {
        List<GraSubject> subjectsByUserId = graSubjectMapper.getSubjectsByUserId(userId);
        if(subjectsByUserId == null || subjectsByUserId.size() <=0){
            return null;
        }
        List<SubjectInfoVO> subjectInfoVO = new ArrayList<>();
        //DO转VO
        for (GraSubject graSubject:subjectsByUserId){
            subjectInfoVO.add(do2SubjectInfo(graSubject));
        }
        return subjectInfoVO;
    }

    @Override
    public List<SubjectInfoVO> getSubjectsByUserId3(int userId) {
        List<GraSubject> subjectsByUserId = graSubjectMapper.getSubjectsByUserId3(userId);
        if(subjectsByUserId == null || subjectsByUserId.size() <=0){
            return null;
        }
        List<SubjectInfoVO> subjectInfoVO = new ArrayList<>();
        //DO转VO
        for (GraSubject graSubject:subjectsByUserId){
            subjectInfoVO.add(do2SubjectInfo(graSubject));
        }
        return subjectInfoVO;
    }
    //编辑课题
    @Override
    public int updateSubject(SubjectInfoVO subjectInfoVO) {
        GraSubject graSubject = do2GraSubject(subjectInfoVO);
        Integer integer = graSubjectMapper.updateById(graSubject);
        return integer;
    }

    @Override
    public int delSubject(int suid) {
        EntityWrapper ew = new EntityWrapper();
        ew.eq("suid",suid);
        Integer delete = graSubjectMapper.delete(ew);
        return delete;
    }

    @Override
    public ShowTeamInfoVO getTeamInfoWithSub(int userId) {
        List<GraSubject> subjectsByUserId = graSubjectMapper.getSubjectsByUserId(userId);
        List<TeamInfoVO> teamInfoVOList = new ArrayList<>();
        ShowTeamInfoVO showTeamInfoVO = new ShowTeamInfoVO();

        for(GraSubject graSubject:subjectsByUserId){
            TeamInfoVO teamInfoVO = new TeamInfoVO();
            teamInfoVO.setSuid(graSubject.getSuid());
            teamInfoVO.setTitle(graSubject.getTitle());
            teamInfoVO.setType(graSubject.getType());
            List<TeamListVO> teamListVOList = graSubjectMapper.getTeamInfo(graSubject.getSuid());
            for(TeamListVO teamListVO:teamListVOList){
                teamListVO.setMembers(graSubjectMapper.getMemberInfo(teamListVO.getTeamId()));
            }
            teamInfoVO.setTeamList(teamListVOList);
            teamInfoVOList.add(teamInfoVO);
        }
        showTeamInfoVO.setContent(teamInfoVOList);
        return showTeamInfoVO;
    }

    @Override
    public ShowTeamInfoVO getMyStu(int userId) {
        //获取我的已选定学生的题目
        List<GraSubject> subjectsByUserId = graSubjectMapper.getSubjectsByUserId2(userId);
        List<TeamInfoVO> teamInfoVOList = new ArrayList<>();
        ShowTeamInfoVO showTeamInfoVO = new ShowTeamInfoVO();

        for(GraSubject graSubject:subjectsByUserId){
            TeamInfoVO teamInfoVO = new TeamInfoVO();
            teamInfoVO.setSuid(graSubject.getSuid());
            teamInfoVO.setTitle(graSubject.getTitle());
            teamInfoVO.setType(graSubject.getType());
            List<TeamListVO> teamListVOList = graSubjectMapper.getTeam(graSubject.getSuid());
            for(TeamListVO teamListVO:teamListVOList){
                    teamListVO.setMembers(graSubjectMapper.getMemberInfo(teamListVO.getTeamId()));
            }
            teamInfoVO.setTeamList(teamListVOList);
            teamInfoVOList.add(teamInfoVO);
        }
        showTeamInfoVO.setContent(teamInfoVOList);
        return showTeamInfoVO;
    }

    @Override
    public int updateInten(int suid, int teamId) {
        int i = graTeamIntentionMapper.chooseStu(suid, teamId);
        return i;
    }

    @Override
    public int delIntention(int suid, int teamId) {
        return graTeamIntentionMapper.delIntention(suid,teamId);
    }

    /**
     * 处理上传的文件
     *
     * @param in
     * @param fileName
     * @return
     * @throws Exception
     */
    @Override
    public List getBankListByExcel(byte[] bytes, String fileName) throws Exception {
        List list = new ArrayList<>();
        InputStream in = new ByteArrayInputStream(bytes);
        //创建Excel工作薄
        Workbook work = this.getWorkbook(in, fileName);
        if (null == work) {
            throw new Exception("创建Excel工作薄为空！");
        }
        Sheet sheet = null;
        Row row = null;
        Cell cell = null;

        for (int i = 0; i < work.getNumberOfSheets(); i++) {
            sheet = work.getSheetAt(i);
            if (sheet == null) {
                continue;
            }

            for (int j = sheet.getFirstRowNum(); j <= sheet.getLastRowNum(); j++) {
                row = sheet.getRow(j);
                if (row == null || row.getFirstCellNum() == j) {
                    continue;
                }

                List<Object> li = new ArrayList<>();
                for (int y = row.getFirstCellNum(); y < row.getLastCellNum(); y++) {
                    cell = row.getCell(y);
                    li.add(cell);
                }
                list.add(li);
            }
        }
        work.close();
        return list;
    }

    /**
     * 判断文件格式
     *
     * @param inStr
     * @param fileName
     * @return
     * @throws Exception
     */
    public Workbook getWorkbook(InputStream inStr, String fileName) throws Exception {
        Workbook workbook = null;
        String fileType = fileName.substring(fileName.lastIndexOf("."));
        if (".xls".equals(fileType)) {
            workbook = new HSSFWorkbook(inStr);
        } else if (".xlsx".equals(fileType)) {
            workbook = new XSSFWorkbook(inStr);
        } else {
            throw new Exception("请上传excel文件！");
        }
        return workbook;
    }

    //subject表VO转DO
    private GraSubject do2GraSubject(SubjectInfoVO subjectInfoVO){
        GraSubject graSubject = new GraSubject();

        graSubject.setSuid(subjectInfoVO.getSuid());
        graSubject.setTitle(subjectInfoVO.getTitle());
        graSubject.setTeacher(subjectInfoVO.getTeacher());
        graSubject.setType(subjectInfoVO.getType());
        graSubject.setContent(subjectInfoVO.getContent());
        graSubject.setSelectedNum(subjectInfoVO.getSelectedNum());
        graSubject.setLimitedNum(subjectInfoVO.getLimitedNum());

        return graSubject;
    }

    //subject表的DO 转 VO
    private SubjectInfoVO do2SubjectInfo(GraSubject graSubject){
        SubjectInfoVO subjectInfoVO = new SubjectInfoVO();

        subjectInfoVO.setSuid(graSubject.getSuid());
        subjectInfoVO.setTitle(graSubject.getTitle());
        subjectInfoVO.setType(graSubject.getType());
        subjectInfoVO.setTeacher(graSubject.getTeacher());
        subjectInfoVO.setSelectedNum(graSubject.getSelectedNum());
        subjectInfoVO.setLimitedNum(graSubject.getLimitedNum());
        subjectInfoVO.setContent(graSubject.getContent());

        return subjectInfoVO;
    }
}
